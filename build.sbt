name := "Helis"

licenses := Seq("BSD 3-Clause License" -> url("https://opensource.org/licenses/BSD-3-Clause"))

homepage := Some(url("http://helis.yexperiment.com"))

scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/yexperiment/helis"),
    "scm:git@bitbucket.org:yexperiment/helis.git"
  )
)

developers := List(
  Developer(
    id    = "Andrzej Tucholka",
    name  = "Y Experiment",
    email = "andrzej@yexperiment.com",
    url   = url("http://www.yexperiment.com")
  )
)

organization := "com.yexperiment"

organizationName := "Y Experiment"

scalaVersion := "2.12.1"

resolvers += Resolver.sonatypeRepo("releases")

libraryDependencies ++= {
  val lwjglVersion = "3.1.1"

  Seq(
    "org.lwjgl" % "lwjgl" % lwjglVersion
    , "org.lwjgl" % "lwjgl-glfw" % lwjglVersion
    , "org.lwjgl" % "lwjgl-jemalloc" % lwjglVersion
    , "org.lwjgl" % "lwjgl-nanovg" % lwjglVersion
    , "org.lwjgl" % "lwjgl-openal" % lwjglVersion
    , "org.lwjgl" % "lwjgl-opengl" % lwjglVersion
    , "org.lwjgl" % "lwjgl-stb" % lwjglVersion
    , "org.lwjgl" % "lwjgl" % lwjglVersion classifier "natives-windows" classifier "natives-linux" classifier "natives-macos"
    , "org.lwjgl" % "lwjgl-glfw" % lwjglVersion classifier "natives-windows" classifier "natives-linux" classifier "natives-macos"
    , "org.lwjgl" % "lwjgl-jemalloc" % lwjglVersion classifier "natives-windows" classifier "natives-linux" classifier "natives-macos"
    , "org.lwjgl" % "lwjgl-nanovg" % lwjglVersion classifier "natives-windows" classifier "natives-linux" classifier "natives-macos"
    , "org.lwjgl" % "lwjgl-openal" % lwjglVersion classifier "natives-windows" classifier "natives-linux" classifier "natives-macos"
    , "org.lwjgl" % "lwjgl-opengl" % lwjglVersion classifier "natives-windows" classifier "natives-linux" classifier "natives-macos"
    , "org.lwjgl" % "lwjgl-stb" % lwjglVersion classifier "natives-windows" classifier "natives-linux" classifier "natives-macos"
  )
}

sonatypeProfileName := "com.yexperiment.helis"

scalacOptions in (Compile,doc) ++= Seq("-groups", "-implicits")

useGpg := true // Install http://www.scala-sbt.org/sbt-pgp/ and configure keys to get past this

import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runClean,
  runTest,
  setReleaseVersion,
  commitReleaseVersion,
  tagRelease,
  ReleaseStep(action = Command.process("sonatypeOpen \"com.yexperiment.helis\" \"Helis staging repository\"", _)),
  ReleaseStep(action = Command.process("publishSigned", _)),
  setNextVersion,
  commitNextVersion,
  ReleaseStep(action = Command.process("sonatypeReleaseAll", _)),
  pushChanges
)