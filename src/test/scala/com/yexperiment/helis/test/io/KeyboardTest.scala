package com.yexperiment.helis.test.io

import com.yexperiment.helis._
import com.yexperiment.helis.io.KeyboardInput
import org.lwjgl.glfw.GLFW._

object KeyboardTest extends ReferenceApplication with KeyboardInput {

  override def onInitWindowCallbacks(window: Long): Unit = initKeyboardCallbacks(window)

  def onKey(window: Long, key: Int, scancode: Int, action: Int, mods: Int): Unit = {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) close(window)
  }

  def onChar(window: Long, codepoint: Int): Unit = {
    logger.info(codepoint.toChar.toString)
  }

  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {}

  def onRender(window: Long, buffers: BufferMap): Unit = {}

}