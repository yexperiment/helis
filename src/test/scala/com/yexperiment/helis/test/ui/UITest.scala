package com.yexperiment.helis.test.ui

import com.yexperiment.helis._
import com.yexperiment.helis.controllers.GuiController
import com.yexperiment.helis.io._
import com.yexperiment.helis.ui.components._
import org.lwjgl.glfw.GLFW._

import scala.collection.mutable

object UITest extends ReferenceApplication with KeyboardInput {

  var fpsTracker = new FpsTracker()

  val guiController = new GuiController(windowWidth, windowHeight, fpsTracker) {
    override def initNodes(): NodeMap = mutable.Map(
      "fpsGraph" -> new FpsGraph(parent = this, fpsTracker = fpsTracker)
      , "header" -> new Header(parent = this) {
        setTitle("UI Test")
      }
    )
  }

  def onKey(window: Long, key: Int, scancode: Int, action: Int, mods: Int): Unit = {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) close(window)
  }

  def onChar(window: Long, codepoint: Int): Unit = {}

  override def onInitWindowCallbacks(window: Long): Unit = {
    initKeyboardCallbacks(window)
  }

  override def initializeBuffers(window: Long): BufferMap = guiController.initialize

  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {
    fpsTracker.frame(timestamp)
    guiController.update(window, buffers, timestamp, frameWidth, frameHeight)
  }

  def onRender(window: Long, buffers: BufferMap): Unit =
    guiController.render(window, buffers)

  override def onFinalize(): Unit =
    guiController.onFinalize()
}

