package com.yexperiment.helis.test

import com.yexperiment.helis._

object ApplicationTest extends ReferenceApplication {
  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {}
  def onRender(window: Long, buffers: BufferMap): Unit = {}
}