package com.yexperiment.helis

import java.io.{FileInputStream, FileOutputStream}
import java.util.logging.Logger

abstract class ReferenceProperties {

  type Props = Map[String, String]

  protected lazy val logger: Logger = Logger.getLogger(getClass.getName)

  private val FILENAME = "runtime.config"

  private var defaults: Props = Map[String, String](
      "name" -> "Helis application",
      "fullscreen" -> false.toString,
      "left" -> (-1).toString,
      "top" -> (-1).toString,
      "width" -> 1920.toString,
      "height" -> 1080.toString,
      "debug" -> false.toString
    )

  initialize()

  def onInitialize: Props

  private def initialize(loadFromFile: Boolean = true): Unit =
    try {
      val props = new java.util.Properties(System.getProperties)
      defaults.foreach({case (key, value) => props.setProperty(key, value)})
      onInitialize.foreach({case (key, value) => props.setProperty(key, value)})

      System.setProperties(props)
      if (loadFromFile) {
        props.load(new FileInputStream(FILENAME))
        System.setProperties(props)
      }
    } catch {
      case e: Exception => logger.finer("Failed to load the runtime properties [" + e.getLocalizedMessage + "] " +
          e.getStackTrace.foldLeft("")(
            (curr, stack) => curr + "\t\t" + stack.toString + "\n"
          )
        )
    }

  def apply(key: String): String = System.getProperty(key, "-1")

  def set(key: String, value: String): Unit = if (defaults.contains(key)) System.setProperty(key, value)

  def save(): Unit =
    try {
      System.getProperties.store(new FileOutputStream(FILENAME), "")
    } catch {
      case e: Exception => logger.finer("Failed to save the runtime properties [" + e.getLocalizedMessage + "] " +
        e.getMessage + "\n" +
        e.getStackTrace.foldLeft("")(
          (curr, stack) => curr + "\t\t" + stack.toString + "\n"
        )
      )
    }
}