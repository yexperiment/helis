package com.yexperiment.helis.ui.controls

import com.yexperiment.helis.BufferMap
import com.yexperiment.helis.ui.UIController
import com.yexperiment.helis.ui.colors.Color
import org.lwjgl.nanovg.NVGColor
import org.lwjgl.nanovg.NanoVG._


class Label(parent: UIController, var text: String, var font: String = "debug", var flags: Int = 0, color: Option[Color] = None) extends UIController(parent) {

  var textColor: NVGColor = NVGColor.create()
  color match {
    case Some(c) => setColor(c)
    case None => setColor(style.colorPalette.active.brightest)
  }

  def textWidth: Int = if (hasHandle) {
    val bounds = new Array[Float](4)
    nvgTextBounds(handle, 0f, 0f, text, bounds)
    (bounds(2) - bounds(0)).toInt
  } else 0

  def setColor(color: Color): Unit = textColor = color.rgba(textColor)

  var fixedLineHeight: Float = -1

  def lineHeight: Float = if (fixedLineHeight > 0) fixedLineHeight else parent.height

  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {}
  def onRender(window: Long, buffers: BufferMap): Unit = {
    nvgFontFace(handle, font)
    nvgFontSize(handle, lineHeight)
    nvgFillColor(handle, textColor)

    nvgTextAlign(handle, flags)

    val m = margins
    val renderX = {
      if ((flags & NVG_ALIGN_RIGHT) > 0) left + width - m.right
      else if ((flags & NVG_ALIGN_CENTER) > 0) left + m.left + (width - m.left - m.right) / 2
      else left + m.left
    }
    val renderY = {
      if ((flags & NVG_ALIGN_BOTTOM) > 0) top + height - m.bottom
      else if ((flags & NVG_ALIGN_MIDDLE) > 0) top + m.top + (height - m.top - m.bottom ) / 2
      else top + m.top
    }
    nvgText(handle, renderX, renderY, text)
  }
}
