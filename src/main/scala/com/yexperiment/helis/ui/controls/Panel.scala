package com.yexperiment.helis.ui.controls

import com.yexperiment.helis.BufferMap
import com.yexperiment.helis.ui.UIController
import com.yexperiment.helis.ui.colors.Color
import org.lwjgl.nanovg.NVGColor
import org.lwjgl.nanovg.NanoVG._

class Panel(parent: UIController, color: Option[Color] = None) extends UIController(parent) {

  private var fillColor: NVGColor = NVGColor.create()
  color match {
    case Some(c) => setColor(c)
    case None => setColor(style.colorPalette.toned.darkest)
  }

  def setColor(color: Color): Unit = fillColor = color.rgba(fillColor)

  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {}
  def onRender(window: Long, buffers: BufferMap): Unit = if (hasHandle) {
    nvgBeginPath(handle)
    val m = margins
    nvgRect(handle, left + m.left, top + m.top, width - m.left - m.right, height - m.top - m.bottom)
    nvgFillColor(handle, fillColor)
    nvgFill(handle)
  }
}
