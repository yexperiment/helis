package com.yexperiment.helis.ui.colors

case class ColorShades(main: Color, brighter: Color, brightest: Color, darker: Color, darkest: Color)
