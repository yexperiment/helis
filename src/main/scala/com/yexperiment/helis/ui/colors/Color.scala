package com.yexperiment.helis.ui.colors

import org.lwjgl.nanovg.NVGColor

case class Color(red: Int = 255, green: Int = 255, blue: Int = 255, alpha: Int = 255) {

  def mult(v: Float) = Color((red * v).toInt, (green * v).toInt, (blue * v).toInt, (alpha * v).toInt)
  def multAlpha(v: Float) = Color(red, green, blue, (alpha * v).toInt)

  def rgba(color: NVGColor): NVGColor = {
    color.r(Math.min(1f, Math.max(0, red) / 255.0f))
    color.g(Math.min(1f, Math.max(0, green) / 255.0f))
    color.b(Math.min(1f, Math.max(0, blue) / 255.0f))
    color.a(Math.min(1f, Math.max(0, alpha) / 255.0f))

    color
  }
}
