package com.yexperiment.helis.ui.colors

case class ColorPalette(
  hot: ColorShades = ColorShades(
    main = Color(216, 0, 0), brighter = Color(252, 33, 33), brightest = Color(252, 82, 82), darker = Color(176, 0, 0), darkest = Color(134, 0, 0)),
  active: ColorShades = ColorShades(
    main = Color(216, 98, 0), brighter = Color(252, 132, 33), brightest = Color(252, 160, 82), darker = Color(176, 80, 0), darkest = Color(134, 81, 0)),
  toned: ColorShades = ColorShades(
    main = Color(0, 130, 130), brighter = Color(20, 151, 151), brightest = Color(52, 162, 162), darker = Color(0, 106, 106), darkest = Color(0, 81, 81)),
  safe: ColorShades = ColorShades(
    main = Color(0, 173, 0), brighter = Color(26, 202, 26), brightest = Color(67, 208, 67), darker = Color(0, 141, 0), darkest = Color(0, 108, 0)))

