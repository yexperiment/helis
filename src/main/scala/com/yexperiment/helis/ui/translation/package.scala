package com.yexperiment.helis.ui

package object translation {
  case class Margins(var left: Int = 0, var top: Int = 0, var right: Int = 0, var bottom: Int = 0) {
    def +(other: Margins) = Margins(
        left = this.left + other.left,
        top = this.top + other.top,
        right = this.right + other.right,
        bottom = this.bottom + other.bottom)
  }

  case class Position(var left: Int = Int.MinValue, var top: Int = Int.MinValue)
  case class Size(var width: Int = -1, var height: Int = -1)
}
