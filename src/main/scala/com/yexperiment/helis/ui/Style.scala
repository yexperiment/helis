package com.yexperiment.helis.ui

import com.yexperiment.helis.ui.colors.ColorPalette

case class Style(colorPalette: ColorPalette = ColorPalette())
