package com.yexperiment.helis.ui

import java.nio.ByteBuffer

import com.yexperiment.helis.BufferMap
import com.yexperiment.helis.controllers.Controller
import com.yexperiment.helis.ui.translation._
import org.lwjgl.system.MemoryUtil.NULL

abstract class UIController(parent: UIController
                            , protected var fixedPosition: Position = Position()
                            , protected var fixedSize: Size = Size()
                            , protected var fixedHandle: Long = NULL
                            , protected var fixedStyle: Style = Style()
                            , protected var fixedFonts: FontMap = Map[String, ByteBuffer]()
                            , protected var fixedMargins: Option[Margins] = None
                           ) extends Controller(parent) {

  def fonts: FontMap = if (parent != null) parent.fonts else fixedFonts
  def style: Style = if (parent != null) parent.style else fixedStyle
  def handle: Long = if (parent != null) parent.handle else fixedHandle
  def hasHandle: Boolean = handle != NULL

  def uiNode(id: String): UIController = nodes(id).asInstanceOf[UIController]

  def margins: Margins = fixedMargins match {
    case Some(margins) => margins
    case _ => if (parent != null) parent.margins else Margins()
  }

  def left: Int = if (fixedPosition.left > Int.MinValue) fixedPosition.left else parent.left
  def top: Int = if (fixedPosition.top > Int.MinValue) fixedPosition.top else parent.top

  def width: Int = if (fixedSize.width > -1) fixedSize.width else parent.width
  def height: Int = if (fixedSize.height > -1) fixedSize.height else parent.height

  override def update(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit =
    if (hasHandle) super.update(window, buffers, timestamp, frameWidth, frameHeight)

  override def render(window: Long, buffers: BufferMap): Unit = if (hasHandle) super.render(window, buffers)

  def setSize(width: Int, height: Int): Unit = {
    fixedSize.width = width
    fixedSize.height = height
  }

  def setPosition(left: Int, top: Int): Unit = {
    fixedPosition.left = left
    fixedPosition.top = top
  }
}
