package com.yexperiment.helis

import java.nio.ByteBuffer

import org.lwjgl.system.MemoryUtil.memUTF8

package object ui {
  type FontMap = Map[String, ByteBuffer]

  private def charToBuf(c: Int) = memUTF8(new String(Character.toChars(c)), false)
  def fa(s: String): ByteBuffer = s match {
    case "fa-times" => charToBuf(0xf00d)
    case "fa-cogs" => charToBuf(0xf085)
    case _ => memUTF8(new String(Character.toChars(0)), false)
  }
}
