package com.yexperiment.helis.ui.components

import com.yexperiment.helis.BufferMap
import com.yexperiment.helis.ui.UIController
import com.yexperiment.helis.ui.controls.{Label, Panel}
import com.yexperiment.helis.ui.translation._
import org.lwjgl.nanovg.NanoVG._

import scala.collection.mutable

class Header(parent: UIController, yScale: Float = 0.05f) extends UIController(parent) {

  override def initNodes(): NodeMap = {
    def offset = (this.height * 0.1f).toInt

    mutable.Map(
      "options" -> new Button(parent = this, text = "Options", icon = "fa-cogs", font = "ui", flags = NVG_ALIGN_RIGHT | NVG_ALIGN_MIDDLE) {
        override def margins: Margins = Margins(0, 0, 2*offset + this.height)
      }
      , "close" -> new Button(parent = this, text = "", icon = "fa-times", font = "ui", flags = NVG_ALIGN_RIGHT | NVG_ALIGN_MIDDLE) {
        override def margins: Margins = Margins(0, 0, offset)
      }
      , "title" -> new Label(parent = this, text = "", font = "ui", flags = NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE) {
        override def lineHeight: Float = this.height * 0.75f
        override def margins: Margins = Margins(left = offset)
      }
      , "background" -> new Panel(parent = this) {
        setColor(style.colorPalette.toned.darkest.multAlpha(0f))
      }
    )
  }

  override def height: Int = (parent.height * yScale).toInt

  def setTitle(text: String): Unit = nodes("title").asInstanceOf[Label].text = text

  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {}
  def onRender(window: Long, buffers: BufferMap): Unit = {}
}
