package com.yexperiment.helis.ui.components

import java.nio.Buffer

import com.yexperiment.helis.ui.UIController
import com.yexperiment.helis.ui.colors.ColorShades
import com.yexperiment.helis.ui.controls.{Label, Panel}
import com.yexperiment.helis.ui.translation._
import com.yexperiment.helis.{BufferMap, FpsTracker}
import org.lwjgl.nanovg.NVGColor
import org.lwjgl.nanovg.NanoVG._

import scala.collection.mutable

class FpsGraph(parent: UIController, fpsTracker: FpsTracker, colorShades: Option[ColorShades] = None) extends UIController(parent) {

  val AVG_FPS = "fps"
  val FPS_HISTORY = "fps_history"

  val scaleToFrame = 0.2f

  override def width: Int = (parent.width * scaleToFrame).toInt
  override def height: Int = (parent.height * scaleToFrame).toInt
  override def top: Int = parent.height - height
  override def left: Int = parent.width - width

  override def initNodes(): NodeMap = mutable.Map(
    "background" -> new Panel(parent = this,
      color = Some({colorShades match {
        case Some(shades) => shades.darkest
        case None => style.colorPalette.safe.darkest
      }}.multAlpha(0.1f))),
    "fpsLabel" -> new Label(parent = this, text = "", font = "debug", flags = NVG_ALIGN_RIGHT | NVG_ALIGN_BOTTOM,
      color = Some({colorShades match {
        case Some(shades) => shades.main
        case None => style.colorPalette.safe.main
      }}.multAlpha(0.9f))) {
      override def lineHeight: Float = this.height * 0.5f
      override def margins: Margins = Margins(right = (this.width * 0.01f).toInt)
    }
  )

  override def onInitialize: BufferMap = {
    Map[String, Buffer](
      //
    ) ++ super.onInitialize
  }

  var vertices = new Array[(Float, Float)](fpsTracker.FRAMES)

  var strokeColor: NVGColor = NVGColor.create()
  colorShades match {
    case Some(shades) => setColorShades(shades)
    case None => setColorShades(style.colorPalette.safe)
  }

  def setColorShades(colorShades: ColorShades): Unit = {
    strokeColor = colorShades.brightest.rgba(strokeColor)
    nodes("background").asInstanceOf[Panel].setColor(colorShades.darkest.multAlpha(0.1f))
    nodes("fpsLabel").asInstanceOf[Label].setColor(colorShades.main.multAlpha(0.9f))
  }

  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {

    uiNode("fpsLabel").asInstanceOf[Label].text = f"${fpsTracker.fps}%.0f"

    def getX(i: Int) = left + i * (width.toFloat / vertices.length.toFloat)
    def getFps(v: Float): Float = 1000f / v
    def getY(v: Float) = top + Math.min(height, Math.max(0, height - getFps(v)))

    val startIndex = fpsTracker.head
    var trackerIndex = startIndex
    var graphIndex = fpsTracker.FRAMES - 1
    do {
      vertices.update(graphIndex, (getX(graphIndex), getY(fpsTracker.history(trackerIndex))))
      trackerIndex = Math.abs((trackerIndex + 1) % fpsTracker.FRAMES)
      graphIndex -= 1
    } while (trackerIndex != startIndex && graphIndex > -1 )
  }


  def onRender(window: Long, buffers: BufferMap): Unit = {
    nvgBeginPath(handle)

    var i = fpsTracker.FRAMES - 1
    nvgMoveTo(handle, vertices(i)._1, vertices(i)._2)
    i -= 1
    while (i > -1) {
      nvgLineTo(handle, vertices(i)._1, vertices(i)._2)
      i -= 1
    }
    nvgStrokeColor(handle, strokeColor)
    nvgStroke(handle)
  }

  override def onFinalize(): Unit = {
  }
}
