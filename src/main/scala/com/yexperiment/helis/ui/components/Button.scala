package com.yexperiment.helis.ui.components

import com.yexperiment.helis.BufferMap
import com.yexperiment.helis.ui._
import com.yexperiment.helis.ui.colors._
import com.yexperiment.helis.ui.controls.{Label, Panel}
import com.yexperiment.helis.ui.translation._
import org.lwjgl.nanovg.NVGColor
import org.lwjgl.nanovg.NanoVG._

import scala.collection.mutable

class Button(parent: UIController,
             text: String, var icon: String = "", font: String = "ui", flags: Int = 0,
             cornerRadius: Float = 2, borderWidth: Float = 2,
             colorShades: Option[ColorShades] = None) extends UIController(parent) {

  lazy val background = new Panel(parent = this,
    color = Some({colorShades match {
      case Some(shades) => shades.darkest
      case None => style.colorPalette.safe.darkest
    }}.multAlpha(0.1f)))

  lazy val caption = new Label(parent = this, text = text, font = font, flags = NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE,
    color = Some({colorShades match {
      case Some(shades) => shades.brightest
      case None => style.colorPalette.safe.brightest
    }}.multAlpha(0.9f))) {

    override def lineHeight: Float = this.height
    override def margins: Margins =
      if (parent.asInstanceOf[Button].icon.isEmpty) Margins() else Margins(left = height)
  }

  override def left: Int =
    if ((flags & NVG_ALIGN_RIGHT) > 0)
      super.left + parent.width - width - margins.right
    else super.left + margins.left

  override def top: Int =
    if ((flags & NVG_ALIGN_BOTTOM) > 0)
      super.top + parent.height - height - margins.bottom
    else super.top + margins.top

  override def width: Int =
    if (caption.text.isEmpty) height
    else caption.textWidth + caption.margins.left + caption.margins.right + {
      if (icon.isEmpty) 0
      else height
    }

  override def initNodes(): NodeMap = mutable.Map(
    "background" -> background,
    "caption" -> caption
  )

  var strokeColor: NVGColor = NVGColor.create()
  colorShades match {
    case Some(shades) => setColorShades(shades)
    case None => setColorShades(style.colorPalette.active)
  }

  def setColorShades(colorShades: ColorShades): Unit = {
    strokeColor = colorShades.main.multAlpha(0.9f).rgba(strokeColor)
    background.setColor(colorShades.darkest.multAlpha(0.1f))
    caption.setColor(colorShades.main.multAlpha(0.9f))
  }

  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {}
  def onRender(window: Long, buffers: BufferMap): Unit = {
    nvgBeginPath(handle)

    val currentMargins = margins
    val renderLeft = left
    val renderTop = top
    val renderWidth = width
    val renderHeight = height

    nvgRoundedRect(handle, renderLeft, renderTop, renderWidth, renderHeight, cornerRadius)
    nvgStrokeColor(handle, strokeColor)
    nvgStrokeWidth(handle, borderWidth)
    nvgStroke(handle)

    if (!icon.isEmpty) {
      nvgFontSize(handle, renderHeight * 0.8f)
      nvgFontFace(handle, "icons")
      nvgFillColor(handle, strokeColor)
      nvgTextAlign(handle, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE)
      nvgText(handle, renderLeft + renderHeight / 2, renderTop + renderHeight / 2, fa(icon))
    }
  }

}
