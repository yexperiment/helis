package com.yexperiment.helis

class FpsTracker {
  val FRAMES = 64
  val history: Array[Long] = new Array[Long](FRAMES)
  var head: Int = 0
  var last: Long = 0

  def frame(current: Long): Long = {
    head = (head + 1) % FRAMES
    if (last == 0) last = current
    val milisBetween = current - last
    history.update(head, milisBetween)
    last = current
    milisBetween
  }

  def fps: Float = 1000 / (history.foldRight(0f)(_ + _) / FRAMES)
}
