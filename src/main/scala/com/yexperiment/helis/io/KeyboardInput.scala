package com.yexperiment.helis.io

import com.yexperiment.helis.ReferenceApplication
import org.lwjgl.glfw.GLFW._

trait KeyboardInput { this: ReferenceApplication =>

  protected def initKeyboardCallbacks(window: Long): Unit = {
    glfwSetKeyCallback(window,
      (window: Long, key: Int, scancode: Int, action: Int, mods: Int) => onKey(window, key, scancode, action, mods))

    glfwSetCharCallback(window, (window: Long, codepoint: Int) => onChar(window, codepoint))
  }

  protected def onChar(window: Long, codepoint: Int): Unit

  protected def onKey(window: Long, key: Int, scancode: Int, action: Int, mods: Int): Unit

}
