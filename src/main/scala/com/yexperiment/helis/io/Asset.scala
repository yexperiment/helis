package com.yexperiment.helis.io

import java.io._
import java.nio._
import java.nio.channels._
import java.nio.file._
import java.util.logging.Logger

import org.lwjgl.BufferUtils._

object Asset {

  protected lazy val logger: Logger = Logger.getLogger(getClass.getName)

  private def loadFromStream(streamPath: String): Option[ByteBuffer] =
    getClass.getClassLoader.getResourceAsStream(streamPath) match {
      case stream: InputStream =>
        Channels.newChannel(stream) match {
          case channel: ReadableByteChannel =>
            createByteBuffer(1) match {
              case buffer: ByteBuffer =>
                var bytes: Int = -1
                var resultBuffer = buffer
                do {
                  bytes = channel.read(resultBuffer)
                  if ( resultBuffer.remaining() == 0 ) {
                    val newBuffer: ByteBuffer = createByteBuffer(resultBuffer.capacity() * 2)
                    buffer.flip()
                    newBuffer.put(resultBuffer)
                    resultBuffer = newBuffer
                  }
                } while (bytes > -1)
                resultBuffer.flip()
                Some(resultBuffer)
              case _ => None
            }
          case _ => None
        }
      case _ => None
    }

  private def loadFromFile(file: Path): Option[ByteBuffer] =
    Files.newByteChannel(file) match {
      case channel: SeekableByteChannel =>
        createByteBuffer(channel.size().toInt + 1) match {
          case buffer: ByteBuffer =>
            while (channel.read(buffer) != -1) {}
            buffer.flip()
            Some(buffer)
          case _ => None
        }
      case _ => None
    }

  def load(assetPath: String): Option[ByteBuffer] = {
    val path = Paths.get(assetPath)

    if (Files.isReadable(path)) loadFromFile(path)
    else loadFromStream(assetPath)
  }

}
