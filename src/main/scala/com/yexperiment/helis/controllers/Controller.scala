package com.yexperiment.helis.controllers

import java.nio.Buffer
import java.util.logging.Logger

import com.yexperiment.helis.BufferMap

import scala.collection._

abstract class Controller(var parent: Controller) {

  type NodeMap = mutable.Map[String, Controller]

  protected lazy val logger: Logger = Logger.getLogger(getClass.getName)

  val nodes: NodeMap = initNodes()
  var visible: Boolean = true
  var enabled: Boolean = true

  def initNodes(): NodeMap = mutable.Map[String, Controller]()

  def initialize: BufferMap = nodes.values.foldRight(onInitialize)((node, c) => c ++ node.initialize)

  def update(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = if (enabled) {
    nodes.values.foreach(_.update(window, buffers, timestamp, frameWidth, frameHeight))
    onUpdate(window, buffers, timestamp, frameWidth, frameHeight)
  }

  def render(window: Long, buffers: BufferMap): Unit = if (visible) {
    nodes.values.foreach(node => node.render(window, buffers))
    onRender(window, buffers)
  }

  def onInitialize: BufferMap = immutable.Map[String, Buffer]()
  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit
  def onRender(window: Long, buffers: BufferMap): Unit
  def onFinalize(): Unit = nodes.foreach(_._2.onFinalize())
}
