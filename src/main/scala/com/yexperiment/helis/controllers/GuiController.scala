package com.yexperiment.helis.controllers

import com.yexperiment.helis._
import com.yexperiment.helis.io.Asset
import com.yexperiment.helis.ui._
import com.yexperiment.helis.ui.translation._
import org.lwjgl.nanovg.NanoVG._
import org.lwjgl.nanovg.NanoVGGL3._
import org.lwjgl.system.MemoryUtil.NULL

abstract class GuiController(windowWidth: Int, windowHeight: Int, fpsTracker: FpsTracker)
  extends UIController(parent = null,
    fixedPosition = Position(0,0), fixedSize = Size(windowWidth, windowHeight)) {

  var pixelRatio: Float = 1

  def initFonts(): FontMap = Map()

  fixedFonts = {
    Map(
      "debug" -> Asset.load("assets/fonts/CourierPrimeSans.ttf").get
      , "ui" -> Asset.load("assets/fonts/NewsCycle.ttf").get
      , "icons" -> Asset.load("assets/fonts/fontawesome-webfont.ttf").get
    ) ++ initFonts()
  }

  private def initAssets() = if (handle != NULL) {
    fonts.foreach({
      case (name, buffer) =>
        if (nvgCreateFontMem(handle, name, buffer, 0) < 0)
          logger.severe("Failed to load font: " + name)
    })
  }

  override def onInitialize: BufferMap = {
    nvgCreate(NVG_ANTIALIAS | NVG_STENCIL_STROKES) match {
      case handle if handle != NULL =>
        fixedHandle = handle
        initAssets()
      case _ => logger.severe("Failed to initialize nanoVG")
    }
    super.onInitialize
  }

  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {
    pixelRatio = frameWidth.toFloat / windowWidth.toFloat
    setSize(frameWidth, frameHeight)
    nvgBeginFrame(handle, frameWidth, frameHeight, pixelRatio)
  }

  def onRender(window: Long, buffers: BufferMap): Unit = {
    nvgEndFrame(handle)
  }

  override def onFinalize(): Unit = if (hasHandle) {
    nvgDelete(handle)
  }

}
