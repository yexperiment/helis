package com.yexperiment.helis.controllers

import com.yexperiment.helis.BufferMap

class SceneController extends Controller(null) {
  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int): Unit = {}
  def onRender(window: Long, buffers: BufferMap): Unit = {}
}
