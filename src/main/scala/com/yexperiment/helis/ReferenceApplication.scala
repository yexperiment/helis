package com.yexperiment.helis

import java.nio.Buffer
import java.util.logging.Logger

import org.lwjgl.glfw.Callbacks._
import org.lwjgl.glfw.GLFW._
import org.lwjgl.glfw._
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11._
import org.lwjgl.system.MemoryUtil
import org.lwjgl.system.MemoryUtil.NULL

abstract class ReferenceApplication(val name: String = "Helis app",
                                    var windowWidth: Int = 1920, var windowHeight: Int = 1080) {

  val FB_WIDTH_INT = "width"
  val FB_HEIGHT_INT = "height"

  private object loopThreadLock
  private var appTerminated = false

  protected lazy val logger: Logger = Logger.getLogger(getClass.getName)

  def initializeBuffers(window: Long): BufferMap = Map[String, Buffer]()
  def onUpdate(window: Long, buffers: BufferMap, timestamp: Long, frameWidth: Int, frameHeight: Int)
  def onRender(window: Long, buffers: BufferMap)
  def onFinalize(): Unit = {}

  def isFullscreen: Boolean = false
  def onConfigureMainWindow(): Unit = {
    glfwDefaultWindowHints()
    glfwWindowHint(GLFW_SAMPLES, 2)
  }
  def onInitWindowCallbacks(window: Long): Unit = {}
  def onMoveMainWindow(window: Long, xpos: Int, ypos: Int): Unit = {}
  def onShowMainWindow(window: Long): Unit = {
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2)
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)
  }
  def onCloseMainWindow(window: Long): Unit = {}
  def onResizeMainWindow(window: Long, newWidth: Int, newHeight: Int): Unit = {}

  def onError(error: Int, description: Long): Unit =
    logger.severe("[GLFW Error " + error.toString + "] " +  GLFWErrorCallback.getDescription(description) + "\n" +
      Thread.currentThread.getStackTrace.foldLeft("")((curr, stack) => curr + "\t\t" + stack.toString)
    )

  def main(args: scala.Array[String]): Unit = apply(args)
  def apply(args: Array[String]): Unit = {
    glfwSetErrorCallback((error: Int, description: Long) => onError(error, description)) match {
      case c: GLFWErrorCallback => c.free()
      case _ => ;
    }

    // TODO: handle input args and configuration

    if (glfwInit()) try {
      onConfigureMainWindow()
      glfwCreateWindow(windowWidth, windowHeight, name, if (isFullscreen) glfwGetPrimaryMonitor() else NULL, NULL) match {
        case window: Long if window != NULL =>
          try {
            initWindowCallbacks(window)
            onShowMainWindow(window)
            glfwShowWindow(window)
            new Thread(() => loop(window)).start() // This starts the main loop that will be stopped on appTerminated flag
            while (!glfwWindowShouldClose(window)) // Keep the main window thread looped until the main window should close
              glfwWaitEvents()
            loopThreadLock.synchronized { appTerminated = true } // Make sure to send the termination flag to the thread with the loop
          } finally {
            glfwFreeCallbacks(window)
            glfwDestroyWindow(window)
          }

        case _ => logger.severe("Failed to create the GLFW window")
      }
    } finally {
      glfwTerminate()
      glfwSetErrorCallback(null).free()
    } else logger.severe("Failed to initialize GLFW")
  }

  def close(window: Long): Unit = {
    glfwSetWindowShouldClose(window, true)
  }

  protected def initWindowCallbacks(window: Long): Unit = {
    glfwSetWindowCloseCallback(window, (window: Long) => onCloseMainWindow(window)) match {
      case c: GLFWWindowCloseCallback => c.free()
      case _ => ;
    }
    glfwSetWindowPosCallback(window, (window: Long, xpos: Int, ypos: Int) => onMoveMainWindow(window, xpos, ypos)) match {
      case c: GLFWWindowPosCallback => c.free()
      case _ => ;
    }

    glfwSetWindowSizeCallback(window,
      (window: Long, newWidth: Int, newHeight: Int) =>
      if (newWidth > 0 && newHeight > 0) {
        windowWidth = newWidth
        windowHeight = newHeight

        onResizeMainWindow(window, newWidth, newHeight)
      }
    ) match {
      case c: GLFWWindowSizeCallback => c.free()
      case _ => ;
    }
    onInitWindowCallbacks(window:Long)
  }

  private def loop(window: Long): Unit = {
    glfwMakeContextCurrent(window)
    GL.createCapabilities()

    val frameWidthBuffer = MemoryUtil.memAllocInt(1)
    val frameHeightBuffer = MemoryUtil.memAllocInt(1)

    val buffers = Map[String, Buffer](
      FB_WIDTH_INT -> frameWidthBuffer,
      FB_HEIGHT_INT -> frameHeightBuffer
    ) ++ initializeBuffers(window) // Define the state buffers to be used in rendering
    try {
      while (!appTerminated)
        try {
          glfwGetFramebufferSize(window, frameWidthBuffer, frameHeightBuffer)
          val frameWidth = frameWidthBuffer.get()
          val frameHeight = frameHeightBuffer.get()

          glViewport(0, 0, frameWidth, frameHeight) // Reset viewport
          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT) // Clear the screen

          val timestamp = System.currentTimeMillis()

          onUpdate(window, buffers, timestamp, frameWidth, frameHeight) // Apply the changes to the buffers
          onRender(window, buffers) // Render the frame based on buffers

          if (!appTerminated) {
            glfwSwapBuffers(window)  // Swap window buffers to display the render
            buffers.foreach({ case (_, b) => b.flip() }) // Flip buffers for next loop
          }
        } catch {
          case e: Exception =>
            glfwSetWindowShouldClose(window, true)
            throw e
        }
    } finally {
      onFinalize()
      buffers.foreach({case (name: String, buffer: Buffer) => MemoryUtil.memFree(buffer)})
    }
  }
}
