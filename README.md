Helis - The scala game framework 
============================

Using the lwjgl game library with nanovg.

Use the Lanucher object (init.Launcher) to execute the com.yexperiment.helis.demo application (desktop package).

### Notices

1. Remember to install and checkout the LFS files as the assets and jars will have trouble:
```git lfs install```